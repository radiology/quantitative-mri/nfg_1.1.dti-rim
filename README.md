# Numerical Fiber Generator
The Numerical Fiber Generator (NFG) is a collection of tools to generate numerical fiber structures with the complexity of human white matter and simulate Diffusion-Weighted MR images that would arise from them (from the [NFG website](https://www.nitrc.org/projects/nfg/)). The tool is distributed under the General Public License (GPL).

The distribution in this repository (nfg.1.1.dti-rim) contains modifications that allow fast generation of 2D data and other signal-related alterations, and is also distributed under the GPL.

# Modifications in version nfg.1.1.dti-rim
All modifications to the original NFG code were restricted to the following files:
- phantom/mri_sim/main.c
- phantom/mri_sim/mri_sim.c
- phantom/mri_sim/mri_sim.h
- phantom/mri_sim/sim_voxel_intensities.c
- phantom/mri_sim/sim_voxel_intensities.h
## Generating 2D images instead of 3D volumes
To accelerate the creation of 2D training data, we modified the NFG code to allow the simulation of the signal only for arbitrary slices (in the *z* direction). The slice index is randomly selected from a uniform distribution and is restricted to slices containing fibers.

This modification allows us to generate data with more variation of the diffusion parameters, while also increasing the variation in anatomy.

## Arbitrary non-diffusion weighted signal
The original NFG code specified the non-diffusion weighted signal S<sub>0</sub>=1. We modified the code to accept user-defined values through the parameter files.

## Removing background signal
The original NFG code assumes that signal is present in all voxels. We modified this behavior so that background voxels only generate residual signal.
#
# Documentation and support

## NFG
For more details on the NFG tool, usage, and examples, please refer to the [NFG website](https://www.nitrc.org/projects/nfg/).

## Usage of version nfg.1.1.dti-rim
This NFG version is intended to be used to generate training data for the [dtiRIM method](https://gitlab.com/radiology/quantitative-mri/emcqmri_dti).

The dtiRIM repository provides Python wrapper functions for a end-to-end data generator, with parameters specific for training the dtiRIM.

#
## Authors and acknowledgment

Thomas G. Close, Jacques-Donald Tournier, Fernando Calamante, Leigh A. Johnston, Iven Mareels and Alan Connelly. "A software tool to generate simulated white matter structures for the assessment of fibre-tracking algorithms". NeuroImage (2009), doi:10.1016/j.neuroimage.2009.03.07/7
#
## License
This NFG version (nfg.1.1.dti-rim) is released under the General Public License - [GPL](GNU_GENERAL_PUBLIC_LICENSE.txt)
